#!/opt/local/bin/python
import re
from collections import defaultdict

def main():
    """
        This is one of the problem sets for the course I TA (Chem170). Enjoyed doing this as an exercise
        in regular expressions (see parse_formula).

        problem: Given a chemical equation, parse out the number of occurences of each element and count them,
        distinguishing between the reactants side and the products side

        applications: balacing chemical equation, calculating reaction quotient, etc.
    """

    # Test cases
    # eq = "C2H5OH -> C2H5OH"
    # eq = "CH3COOH -> H + CH3COO"
    # eq = "NaCl -> Na + Cl"
    # eq = "2CH3OH + 3O2 -> 2CO2 + 4H2O"
    # eq = "8H + MnO4 + 2I -> Mn + I2 + 4H2O"
    # eq = "Se + Cr(OH)3 -> Cr + SeO3 + 3H"
    # eq = "2Na3PO4 + 3CuCl2 -> Cu3(PO4)2 + 6NaCl"
    # eq = "(NH4)3PO4 + 3KCl -> 3NH4Cl + K3PO4"
    # eq = "Ag + 2NH3 -> Ag(NH3)2"
    eq = "3Hg2(NO3)2 + 4Na2HPO4 -> (Hg2)3(PO4)2 + 6NaNO3 + 2NaH2PO4"

    reactants, products = parse_eq(eq)

    print "Reactants:"
    pretty_print(count_elts(reactants))
    print "Products:"
    pretty_print(count_elts(products))

def parse_eq(eq):
    """
    Converts given chemical equation (str) into a list of strs that are the molecules in the reactants and products side
    :param eq: str chemical eq representation
    :return: tuple containing (reax_lst, prods_lst)
    """
    reactants, products = map(str.split, eq.split('->'))

    return reactants[::2], products[::2]

def count_elts(molecules):
    """
    Given a list of molecules, returns a dictionary containing the count among all the molecules
    :param molecules: list of strings, each being a molecule
    :return: dictionary with count
    """
    count = {}
    for mol in molecules:
        d = parse_formula(mol)
        # Get lead coeff, if not there, just make it a one
        try:
            lead = int(re.findall('^\d+', mol)[0])
        except IndexError:
            lead = 1
        for elt in d:
            # Multiply by leading coeff
            d[elt] *= lead

            # Update the count dict
            if elt in count:
                count[elt] += d[elt]
            else:
                count[elt] = d[elt]
    return count


def parse_formula(formula):
    """
    Given a chemical formula for a molecule (str), no leading coeff, returns a dictionary with the proper count
    :param formula: str that is a molecule
    :return: dictionary with count of each element
    """
    counts = defaultdict(int)
    for term in re.findall("[A-Z][a-z]?[0-9]*|\(\w+\)[0-9]+", formula):
        result = re.match("\((\w+)\)([0-9]+)$", term)
        if result:
            for element, count in parse_formula(result.group(1)).items():
                counts[element] += int(result.group(2)) * count
        else:
            result = re.match("([A-Z][a-z]?)([0-9]*)", term)
            if result.group(2) == '':
                counts[result.group(1)] += 1
            else:
                counts[result.group(1)] += int(result.group(2))

    return counts

def pretty_print(elt_count):
    for k,v in sorted(elt_count.iteritems()):
        print "Chemical Symbol = %s, frequency = %d" % (k, v)
    print '\n'

if __name__ == '__main__':
    main()
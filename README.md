# Chem170 Problem Sets #

This repo is the hub for my solutions to the problem sets for CHEM170 (Computer Programming for Chemists/Cheminformatics), a course I TA for.

### Details ###

* The course was first taught at Pomona the Spring 2017 semester
* I will only post my solutions to problem sets after they are due
* [FMI](https://aspc.pomona.edu/courses/browse/instructor/2046/course/CHEM170-PO/)